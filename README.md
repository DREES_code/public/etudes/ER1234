# ER1234
Ce dossier fournit les programmes permettant de reproduire la méthodologie et les figures de l'Études et résultats n° 1234 de la DREES : « L'opinion des Français sur les inégalités reflète-t-elle leur position sur l'échelle des revenus ? », juin 2022.

Un ensemble de 7 programmes est mis à disposition :
- Le programme `initialisation.R` définit l’environnement, les chemins, charge les données et les fonctions, et appelle les autres codes. L’utilisateur remplacera la localisation de l’environnement de travail **setwd** par son propre chemin d’accès aux programmes, **path** par le chemin d’accès aux données du Baromètre et **path_erfs** par le chemin d'accès aux ERFS.
- Le programme `seuils_revenu.R` calcule des déciles de revenu à partir des ERFS.
- Le programme `data.R` effectue le traitement des données du Baromètre.
- Le programme `biais_standard.R` construit la variable de biais standardisé.
- Les programmes `graphiques.R`, `tableaux.R` et `annexe_technique.R` effectuent les principales analyses de l'étude et exportent dans le dossier « excel » les données nécessaires à la création des figures de l’ER.


Lien vers l'étude : https://drees.solidarites-sante.gouv.fr/publications-communique-de-presse/etudes-et-resultats/lopinion-des-francais-sur-les-inegalites

La Direction de la recherche, des études, de l’évaluation et des statistiques (Drees) est une direction de l’administration centrale des ministères sanitaires et sociaux. https://drees.solidarites-sante.gouv.fr/etudes-et-statistiques/la-drees/qui-sommes-nous/ 

Source de données : Baromètre d'opinion de la DREES. Traitements : Drees. Une présentation du Baromètre, les questionnaires et les jeux de données sont disponibles en accès libre : https://drees.solidarites-sante.gouv.fr/sources-outils-et-enquetes/le-barometre-dopinion-de-la-drees


Les programmes ont été exécutés pour la dernière fois avec le logiciel R version 4.1.2, le 17/06/2022.
